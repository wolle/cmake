genexp-no-eval
--------------

* :manual:`generator expressions <cmake-generator-expressions(7)>`
  short-circuit to avoid unnecessary evaluation of parameters.
